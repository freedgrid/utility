#define WIN32_LEAN_AND_MEAN 1
#include <stdlib.h>
#include <windows.h>
#include <strsafe.h>
#include <malloc.h>
#include <stdio.h>
#include <iostream>>

//для работы со счетчиками производительности:
#include <pdh.h>
#include <pdhmsg.h>
#pragma comment(lib, "pdh.lib")

//для получения информации о процессах:
#include "psapi.h"
#include "Shlwapi.h"
#pragma comment(lib, "Shlwapi.lib")

#include <set>
#include <vector>

const PWSTR COUNTER_OBJECT = L"GPU Engine"; //Имя счетчика производительности

											/*Вывод имени процесса по его PID*/
std::wstring PrintProcessName(int pid) {

	HANDLE hProcess;
	DWORD charsCarried = 0;
	TCHAR PrName[MAX_PATH] = L"";

	hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);
	if (hProcess)
	{
		charsCarried = MAX_PATH;
		if (QueryFullProcessImageName(hProcess, 0, PrName, &charsCarried) == 0) {
			StringCchCopy(PrName, MAX_PATH, L"Unknown process");
		}

		CloseHandle(hProcess);
	}
	else StringCchCopy(PrName, MAX_PATH, L"Unknown process");

	PathStripPath(PrName);
	wprintf(L"%s", PrName);
	std::wstring str(PrName);
	return str;
}

void KillProcess(int id)
{
	HANDLE ps = OpenProcess(1, false, id);
	if (ps)
		TerminateProcess(ps, -9);
}

int averageLoad(const std::vector<int> &p_v_load) {
	int res = 0;
	for (int i = 0; i < p_v_load.size(); i++) {
		res += p_v_load[i];
	}
	if (p_v_load.size())
		res = res / p_v_load.size();
	else
		res = 0;
	return res;
}

void main(void)
{
	std::set<int> set_pids;
	std::vector<int> v_load;
	std::vector<int> v_load1;

	unsigned int Count = 0;

	while (true) {
		PDH_STATUS status = ERROR_SUCCESS;
		LPWSTR pwsCounterListBuffer = NULL;
		DWORD dwCounterListSize = 0;
		LPWSTR pwsInstanceListBuffer = NULL;
		DWORD dwInstanceListSize = 0;
		LPWSTR pTemp = NULL;

		HQUERY hQuery = NULL;
		HCOUNTER hCounter = NULL;

		DWORD dwFormat = PDH_FMT_LONG;
		PDH_FMT_COUNTERVALUE ItemBuffer;
		wchar_t buffer[1000];
		wchar_t * p;
	
	
		// Determine the required buffer size for the data. 
		status = PdhEnumObjectItems(
			NULL,                   // real-time source
			NULL,                   // local machine
			COUNTER_OBJECT,         // object to enumerate
			pwsCounterListBuffer,   // pass NULL and 0
			&dwCounterListSize,     // to get required buffer size
			pwsInstanceListBuffer,
			&dwInstanceListSize,
			PERF_DETAIL_WIZARD,     // counter detail level
			0);

	
		//std::cout << "////////////////////" << std::endl;
		if (status == PDH_MORE_DATA)
		{
			// Allocate the buffers and try the call again.
			pwsCounterListBuffer = (LPWSTR)malloc(dwCounterListSize * sizeof(WCHAR));
			pwsInstanceListBuffer = (LPWSTR)malloc(dwInstanceListSize * sizeof(WCHAR));

			if (NULL != pwsCounterListBuffer && NULL != pwsInstanceListBuffer)
			{
				status = PdhEnumObjectItems(
					NULL,                   // real-time source
					NULL,                   // local machine
					COUNTER_OBJECT,         // object to enumerate
					pwsCounterListBuffer,
					&dwCounterListSize,
					pwsInstanceListBuffer,
					&dwInstanceListSize,
					PERF_DETAIL_WIZARD,     // counter detail level
					0);

				if (status == ERROR_SUCCESS)
				{

					//wprintf(L"\nProcesses using 3D engine:\n\n");

					// Walk the instance list. The list can contain one
					// or more null-terminated strings. The list is terminated
					// using two null-terminator characters.
					//while(true)

					for (pTemp = pwsInstanceListBuffer; *pTemp != 0; pTemp += wcslen(pTemp) + 1)
					{
						//p = wcsstr(pTemp, L"engtype_3D");
						//p = wcsstr(pTemp, L"engtype_Copy");
						p = wcsstr(pTemp, L"engtype_Compute_0");
						if (p == NULL)continue;//отфильтровать записи, не относящиеся к 3D Engine

											   //построим путь к запрашиваемому значению счетчика...
						StringCchCopy(buffer, 1000, L"\\GPU Engine(");
						StringCchCat(buffer, 1000, pTemp);
						//StringCchCat(buffer, 1000, L")\\Running time");
						StringCchCat(buffer, 1000, L")\\Utilization Percentage");

						//создадим запрос...
						status = PdhOpenQuery(NULL, 0, &hQuery);
						if (ERROR_SUCCESS != status)
						{
							wprintf(L"PdhOpenQuery failed with 0x%x\n", status);
							continue;
						}

						//добавим счетчик в запрос...
						status = PdhAddCounter(hQuery, buffer, 0, &hCounter);
						if (ERROR_SUCCESS != status)
						{
							wprintf(L"PdhAddCounter failed with 0x%x\n", status);
							continue;
						}

						//получаем данные...
						status = PdhCollectQueryData(hQuery);
						if (ERROR_SUCCESS != status)
						{
							wprintf(L"PdhCollectQueryData failed with 0x%x\n", status);
							continue;
						}

						Sleep(1000);
						status = PdhCollectQueryData(hQuery);
						if (ERROR_SUCCESS != status)
						{
							wprintf(L"PdhCollectQueryData failed with 0x%x\n", status);
							continue;
						}

						// Format the performance data record.
						status = PdhGetFormattedCounterValue(hCounter, dwFormat, (LPDWORD)NULL, &ItemBuffer);

						if (ERROR_SUCCESS != status)
						{
							wprintf(L"PdhGetFormattedCounterValue failed with 0x%x\n", status);
							continue;
						}

						int pid = 0;

						//выведем информацию о процессе, если Running time > 0
						if (ItemBuffer.longValue > 0) {
							swscanf_s(pTemp, L"pid_%d", &pid);  //парсим PID из имени счетчика                                              
							std::wstring str = PrintProcessName(pid);
							if (!str.compare(L"xmr-stak.exe")) {
								v_load.push_back((int)ItemBuffer.longValue);
							}
							if (!str.compare(L"xmr-stak1.exe")) {
								v_load1.push_back((int)ItemBuffer.longValue);
							}
							wprintf(L", PID: %d - ", pid);
							wprintf(L"Usage time: %d\n", (int)ItemBuffer.longValue);

							set_pids.insert(pid);
						}
						PdhCloseQuery(hQuery);

					}
				}
				else
				{
					wprintf(L"Second PdhEnumObjectItems failed with %0x%x.\n", status);
				}
			}
			else
			{
				wprintf(L"Unable to allocate buffers.\n");
				status = ERROR_OUTOFMEMORY;
			}
		}
		else
		{
			wprintf(L"\nPdhEnumObjectItems failed with 0x%x.\n", status);
		}

		if (pwsCounterListBuffer != NULL) {
			free(pwsCounterListBuffer);
			pwsCounterListBuffer = NULL;
		}

		if (pwsInstanceListBuffer != NULL) {
			free(pwsInstanceListBuffer);
			pwsInstanceListBuffer = NULL;
		}

		if (!(v_load.size() % 10) && v_load.size()) {
			std::cout << "///////////////////" << std::endl;
			std::cout << "average load: " << averageLoad(v_load) << std::endl;
			std::cout << "///////////////////" << std::endl;
		}

		if (!(v_load1.size() % 10) && v_load1.size()) {
			std::cout << "///////////////////" << std::endl;
			std::cout << "average load_1: " << averageLoad(v_load1) << std::endl;
			std::cout << "///////////////////" << std::endl;
		}

		//std::cout << "/////////////////////n" << std::endl;
	}

	//KillProcess(13612);

	system("PAUSE");
}